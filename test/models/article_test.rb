require 'test_helper'

class ArticleTest < ActiveSupport::TestCase

  test "should not save article without body" do
    article = Article.new
    article.title = "First article"
    assert_not article.save, "Saved the article without a body"
  end

end
