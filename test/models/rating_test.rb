require 'test_helper'

class RatingTest < ActiveSupport::TestCase

  def setup
    @article = articles(:one)
  end

  test "should return rating average" do
    assert_equal 3.5, @article.ratings.average(:score).round(2), "Return the exact rating average"
  end

end
