class AddArticleIdToRatings < ActiveRecord::Migration[5.2]
  def change
    add_column :ratings, :article_id, :integer
  end
end
