require 'csv'

class Article < ApplicationRecord

  has_many :comments, dependent: :destroy
  has_many :ratings, dependent: :destroy
  validates :title, presence: true, length: {minimum: 5}
  validates :text, presence: true, length: {minimum: 10}

  def self.to_csv(options = {})
    desired_columns = ["title", "ratingMin", "ratingMax", "ratingAvg", "commentNb", "commentAvgChar"]
    CSV.generate(options) do |csv|
      csv << desired_columns
      all.each do |article|
        title = article.title
        ratingMin = article.ratings.minimum(:score)
        ratingMax = article.ratings.maximum(:score)
        ratingAvg = article.ratings.average(:score).round(2)
        commentNb = article.comments.count()
        commentNbChar = 0
        
        article.comments.each do |comment|
          commentNbChar += comment.body.length
        end

        if commentNb != 0
          commentAvgChar = commentNbChar / commentNb
        else
          commentAvgChar = 0
        end


        csv << [title, ratingMin, ratingMax, ratingAvg, commentNb, commentAvgChar]
      end
    end
  end




end
