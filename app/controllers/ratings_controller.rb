class RatingsController < ApplicationController

  before_action :find_article

  def new
    @rating = Rating.new
  end

  def create
    @rating = Rating.new(rating_params)
    @rating.article_id = @article.id

    if @rating.save
      redirect_to article_path(@article)
    else
      render 'new'
    end
  end

  private

  def rating_params
    params.require(:rating).permit(:score)
  end

  def find_article
    @article = Article.find(params[:article_id])
  end

end
